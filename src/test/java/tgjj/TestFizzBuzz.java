/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package tgjj;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

//GWT Format

// 1. input not divisible by three or five returns input as string
//
//    Given the number 2,
//    When I call FizzBuzz ,
//    Then output is "2".
//
//
// 2. input divisible by three but not five returns "Fizz"
//
//    Given the number 6,
//    When I call FizzBuzz,
//    Then output is "Fizz".
//

//
//


class  TestFizzBuzz{

    @Test void input_not_divisble_by_three_or_five_returns_input_as_string() {

      //arrange
      int number = 2;
      String result;

      //act
      result = FizzBuzz.checkNumber(number);

      //assert
      assertEquals("2", result);
    }

      // 2. input divisible by three but not five returns "Fizz"
      //
      //    Given the number 6,
      //    When I call FizzBuzz,
      //    Then output is "Fizz".

      @Test void input_divisible_by_3_but_not_five_returns_Fizz() {

        //arrange
        int number = 6;
        String result;

        //act
        result = FizzBuzz.checkNumber(number);

        //Assert
        assertEquals("Fizz", result);
      }

      // 3. input divisible by five but not three returns "Buzz"
      //
      //    Given the number 10,
      //    When I call FizzBuzz,
      //    Then output is "Buzz".

      @Test void input_divisible_by_five_but_not_three_returns_Buzz() {

        //arrange
        int number = 10;
        String result;

        //act
        result = FizzBuzz.checkNumber(number);

        //Assert
        assertEquals("Buzz", result);

      }

      // 4. input divisible by three and five returns "FizzBuzz"
      //
      //    Given the number 15,
      //    When I call FizzBuzz,
      //    Then output is "FizzBuzz".

      @Test void input_divisible_by_five_and_three_returns_FizzBuzz() {

        //arrange
        int number = 15;
        String result;

        //act
        result = FizzBuzz.checkNumber(number);

        //Assert
        assertEquals("FizzBuzz", result);

      }

      // 5. input must be a positive integer
      //
      //    Given a negative number,
      //    When I call FizzBuzz,
      //    Then output is an Error

      @Test void input_is_not_a_positive_integer_returns_error() {

        //arrange
        int number = -5;
        String result;

        //act
        result = FizzBuzz.checkNumber(number);

        //Assert
        assertEquals("Test failure", result);
      }

}
